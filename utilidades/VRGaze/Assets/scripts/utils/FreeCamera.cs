﻿using UnityEngine;
using System.Collections;

//////////////////////////////////////////////////////////////////////////////////////
//                                                                                  //
//                                                                                  //
//                                                                                  //  
//////////////////////////////////////////////////////////////////////////////////////
public class FreeCamera : MonoBehaviour
{
	public float cameraSensitivity = 300;
	private float rotationX = 0.0f;
	private float rotationY = 0.0f;
    
	//////////////////////////////////////////////////////////////////////////////////////
	//                                                                                  //
	//                                                                                  //
	//                                                                                  //  
	//////////////////////////////////////////////////////////////////////////////////////
	void Update ()
	{
		rotationX += Input.GetAxis ("Mouse X") * cameraSensitivity * Time.deltaTime;
		rotationY += Input.GetAxis ("Mouse Y") * cameraSensitivity * Time.deltaTime;
		rotationY = Mathf.Clamp (rotationY, -90, 90);
      
		transform.localRotation = Quaternion.AngleAxis (rotationX, Vector3.up);
		transform.localRotation *= Quaternion.AngleAxis (rotationY, Vector3.left);
      
//      if (Input.GetKeyDown (KeyCode.End))
//      {
//        Screen.lockCursor = (Screen.lockCursor == false) ? true : false;
//      }
	}
}