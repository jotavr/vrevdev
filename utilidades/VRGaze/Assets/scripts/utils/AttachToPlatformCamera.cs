﻿using UnityEngine;
using System.Collections;

public class AttachToPlatformCamera : MonoBehaviour {

	public GameObject m_OculusCameraRoot;
	public GameObject m_OculusCameraLeft;
	public GameObject m_OculusCameraRight;

	public GameObject m_CardboardCameraRoot;
	public GameObject m_CardboardCameraLeft;
	public GameObject m_CardboardCameraRight;

	public bool m_bAttachToRoot;
	public bool m_bAttachToLeft;
	public bool m_bAttachToRight;

	////////////////////////////////////////////////////////////////////////////////
	//                                                                            //
	//                                                                            //
	//                                                                            //  
	////////////////////////////////////////////////////////////////////////////////
	void Awake () {
		if (m_OculusCameraRoot.activeInHierarchy) {
			if (m_bAttachToRoot)
				gameObject.transform.SetParent (m_OculusCameraRoot.transform, true);
			else if (m_bAttachToLeft) {
				gameObject.transform.SetParent (m_OculusCameraLeft.transform, true);
			} else {
				gameObject.transform.SetParent (m_OculusCameraRight.transform, true);
			}
		} else {
			if (m_bAttachToRoot) {
				gameObject.transform.SetParent (m_CardboardCameraRoot.transform, true);
			}else if (m_bAttachToLeft) {
				gameObject.transform.SetParent (m_CardboardCameraLeft.transform, true);
			} else {
				gameObject.transform.SetParent (m_CardboardCameraRight.transform, true);
			}
		}
	}


}
