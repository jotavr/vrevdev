﻿// From http://popupasylum.co.uk/?p=945
// Unity Preview Frustum Culling

using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;

[ExecuteInEditMode]
public class PreviewCulling : MonoBehaviour {
	
	#if UNITY_EDITOR
	
	static Camera sceneReferenceCamera;
	
	Vector3 pos;
	Quaternion rot;
	float near;
	float far;
	
	Camera sceneCamera{
		get{
			return GetSceneCamera();
		}
	}
	
	Camera isSceneCamera;
	
	void OnEnable() {
		if (sceneCamera && gameObject != sceneCamera.gameObject) {
			sceneReferenceCamera = GetComponent<Camera>();
			UpdateSceneCamera();
		}
	}
	
	void OnDisable() {
		if (sceneCamera && gameObject != sceneCamera.gameObject) {
			CleanSceneCamera();
			sceneReferenceCamera = null;
		}
	}
	
	private Camera GetSceneCamera() {
		SceneView SceneView = EditorWindow.GetWindow<SceneView>();
		return SceneView.camera;
	}
	
	private void UpdateSceneCamera() {
		//ad a PreviewCulling script to the scene camera if not done already
		if (sceneCamera && !sceneCamera.GetComponent<PreviewCulling>()) {
			sceneCamera.gameObject.AddComponent<PreviewCulling>().isSceneCamera = sceneCamera;
		}
	}
	
	void CleanSceneCamera() {
		if (sceneCamera) {
			//remove the PreviewCulling script from the scene camera
			PreviewCulling pc = sceneCamera.GetComponent<PreviewCulling>();
			if (pc) {
				DestroyImmediate(pc);
			}
		}
	}
	
	void OnPreCull() {
		//if the scene camera is about to cull, save current transform values and copy from the reference camera
		if (isSceneCamera) { 
			if (sceneReferenceCamera) {
				pos  = isSceneCamera.transform.position;
				rot  = isSceneCamera.transform.rotation;
				near = isSceneCamera.nearClipPlane;
				far  = isSceneCamera.farClipPlane;
				isSceneCamera.projectionMatrix = sceneReferenceCamera.projectionMatrix;
				isSceneCamera.transform.position = sceneReferenceCamera.transform.position;
				isSceneCamera.transform.rotation = sceneReferenceCamera.transform.rotation;
				isSceneCamera.nearClipPlane = sceneReferenceCamera.nearClipPlane;
				isSceneCamera.farClipPlane = sceneReferenceCamera.farClipPlane;
			}
		}
	}
	
	void OnPreRender() {
		//if the scene camera is about to render reset its transform to saved values
		if (isSceneCamera) {
			isSceneCamera.transform.position = pos;
			isSceneCamera.transform.rotation = rot;
			isSceneCamera.nearClipPlane = near;
			isSceneCamera.farClipPlane = far;
			isSceneCamera.ResetProjectionMatrix();
		}
	}
	#endif
}