using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//    When gazing over an interactive gameobject, it launches a visual counter//
//    that "clicks" at m_SelectionDuration seconds                            //
//                                                                            //
//                                                                            //  
////////////////////////////////////////////////////////////////////////////////
public class RadialTrigger : MonoBehaviour
{
    public event Action OnSelectionComplete;                                             // This event is triggered when the bar has filled.


    [SerializeField] private float m_SelectionDuration = 2f;                             // How long it takes for the bar to fill.
    [SerializeField] private Image m_radial;                                             // Reference to the image who's fill amount is adjusted to display the bar.
	[SerializeField] private GameObject m_selectionObject;                               // GameObject to activate when gazing an interacti
	[SerializeField] private VRStandardAssets.Utils.VREyeRaycaster m_eyeRaycaster;

    private Coroutine m_SelectionFillRoutine;                                            // Used to start and stop the filling coroutine based on input.
    private bool m_IsSelectionRadialActive;                                              // Whether or not the bar is currently usable.
    
	public float SelectionDuration { get { return m_SelectionDuration; } }

	////////////////////////////////////////////////////////////////////////////////
	//                                                                            //
	//                                                                            //
	//                                                                            //  
	////////////////////////////////////////////////////////////////////////////////
	void Update()
	{
		if (m_IsSelectionRadialActive && (m_eyeRaycaster.CurrentInteractible == null)) {
			Hide ();
		} else if (!m_IsSelectionRadialActive && (m_eyeRaycaster.CurrentInteractible != null)) {
			Show ();
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	//                                                                            //
	//                                                                            //
	//                                                                            //  
	////////////////////////////////////////////////////////////////////////////////
    public void Show()
    {
		if ( m_selectionObject != null )
		  m_selectionObject.gameObject.SetActive(true);
        m_IsSelectionRadialActive = true;
		m_SelectionFillRoutine = StartCoroutine(FillSelectionRadial());
    }

	////////////////////////////////////////////////////////////////////////////////
	//                                                                            //
	//                                                                            //
	//                                                                            //  
	////////////////////////////////////////////////////////////////////////////////
    public void Hide()
    {
		if ( m_selectionObject != null )
		  m_selectionObject.gameObject.SetActive(false);
        m_IsSelectionRadialActive = false;

		if(m_SelectionFillRoutine != null)
			StopCoroutine(m_SelectionFillRoutine);
		
		m_radial.fillAmount = 0f;
	}

	////////////////////////////////////////////////////////////////////////////////
	//                                                                            //
	//                                                                            //
	//                                                                            //  
	////////////////////////////////////////////////////////////////////////////////
    private IEnumerator FillSelectionRadial()
    {
        // Create a timer and reset the fill amount.
        float timer = 0f;
        m_radial.fillAmount = 0f;
        
        // This loop is executed once per frame until the timer exceeds the duration.
        while (timer < m_SelectionDuration)
        {
            // The image's fill amount requires a value from 0 to 1 so we normalise the time.
            m_radial.fillAmount = timer / m_SelectionDuration;

            // Increase the timer by the time between frames and wait for the next frame.
            timer += Time.deltaTime;
            yield return null;
        }

        // When the loop is finished set the fill amount to be full.
        m_radial.fillAmount = 1f;

        // Turn off the radial so it can only be used once.
        m_IsSelectionRadialActive = false;

		if (m_eyeRaycaster.CurrentInteractible != null) {
			m_eyeRaycaster.CurrentInteractible.Click();
		}

        // If there is anything subscribed to OnSelectionComplete call it.
        if (OnSelectionComplete != null)
            OnSelectionComplete();
		Hide ();
    }


}
