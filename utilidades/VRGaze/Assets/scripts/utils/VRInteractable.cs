﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//    Launches UnityEvents from VRInteractiveItem events                      //
//    Makes possible to set actions from Unity inspector to Gaze Enter, exit  //
//    and click VRInteractiveItem events                                      //
//                                                                            //
//                                                                            //  
////////////////////////////////////////////////////////////////////////////////
public class VRInteractable : VRStandardAssets.Utils.VRInteractiveItem {

	public UnityEvent OnTriggerEnter;
	public UnityEvent OnTriggerExit;
	public UnityEvent OnTriggerStayAndClick;

	////////////////////////////////////////////////////////////////////////////////
	//                                                                            //
	//                                                                            //
	//                                                                            //  
	////////////////////////////////////////////////////////////////////////////////
	void Awake()
	{
		OnOver += OnTriggerEnter.Invoke;
		OnOut  += OnTriggerExit.Invoke;
		OnClick+= OnTriggerStayAndClick.Invoke;
	}

}
